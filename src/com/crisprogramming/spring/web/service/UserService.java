/**
 * 
 */
package com.crisprogramming.spring.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import com.crisprogramming.spring.web.dao.User;
import com.crisprogramming.spring.web.dao.UsersDao;

/**
 * @author cristina.smaranda
 *
 */

@Service("userService")
public class UserService {

	private UsersDao usersDao;

	public UsersDao getUsersDao() {
		return usersDao;
	}

	@Autowired
	public void setUsersDAO(UsersDao usersDao) {
		this.usersDao = usersDao;
	}

	public void create(User user) {
		usersDao.create(user);
	}

	public boolean exists(String username) {
		// TODO Auto-generated method stub
		return usersDao.exists(username);
	}

	@Secured({("ROLE_ADMIN")})
	public List<User> getAllUsers() {
		
		return usersDao.getAllUsers();
	}

}
