/**
 * 
 */
package com.crisprogramming.spring.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import com.crisprogramming.spring.web.dao.Offer;
import com.crisprogramming.spring.web.dao.OffersDao;

/**
 * @author cristina.smaranda
 *
 */

@Service("offerService")
public class OfferService {

	private OffersDao offersDao;

	public OffersDao getOffersDAO() {
		return offersDao;
	}

	@Autowired
	public void setOffersDAO(OffersDao offersDao) {
		this.offersDao = offersDao;
	}

	public List<Offer> getCurrent() {
		return offersDao.getOffers();
	}

	public void throwTestException() {
		offersDao.getOffer(000);
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	public void create(Offer offer) {
		offersDao.create(offer);
	}
}
