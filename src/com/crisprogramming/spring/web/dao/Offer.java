/**
 * 
 */
package com.crisprogramming.spring.web.dao;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.crisprogramming.spring.web.validation.ValidEmail;

/**
 * @author cristina.smaranda
 *
 */
public class Offer {

	private int id;
	
	@NotNull
	@Size(max=100, min=5, message="Name must be between 5 and 100 characters")
	private String name;
	
	@NotNull
	//@Pattern(regexp=".*\\@.*\\..*", message=" This doesn't appear to be a valid email address")
	@ValidEmail(min=6, message="Wrong email format")
	private String email;
	
	@Size(max=100, min=5, message="The text must be between 5 and 100 characters")
	private String text;
	
	public Offer() {
		
	}

	public Offer(int id, String name, String email, String text) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.text = text;
	}
	
	public Offer(String name, String email, String text) {		
		this.name = name;
		this.email = email;
		this.text = text;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Offer [id=" + id + ", name=" + name + ", email=" + email + ", text=" + text + "]";
	}

}
