/**
 * 
 */
package com.crisprogramming.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * @author cristina.smaranda
 *
 */
@Component("offersDao")
public class OffersDao {

	// private JdbcTemplate jdbc;
	private NamedParameterJdbcTemplate jdbc;

	public OffersDao() {
		System.out.println("Successfully loaded offerDAO");
	}
	//@Autowired
	public void setDataSource(DataSource jdbc) {
		// this.jdbc = new JdbcTemplate(jdbc);
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);

	}
	
	

	public List<Offer> getOffers() {

		// MapSqlParameterSource params = new MapSqlParameterSource("name",
		// "Sue");
		// MapSqlParameterSource params = new MapSqlParameterSource();
		// params.addValue("name", "Sue");
		return jdbc.query("select * from offers", new RowMapper<Offer>() {

			public Offer mapRow(ResultSet rs, int rowNum) throws SQLException {
				Offer offer = new Offer();

				offer.setId(rs.getInt("id"));
				offer.setName(rs.getString("name"));
				offer.setText(rs.getString("text"));
				offer.setEmail(rs.getString("email"));

				return offer;
			}

		});
		// return null;
	}

	/**
	 * Returns an object Offer for a given id
	 * 
	 * @param id
	 * @return
	 */
	public Offer getOffer(int id) {

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);

		// don't need to cast it to Offer because I specified <offer> for
		// RowMapper generic interface
		return jdbc.queryForObject("select * from offers where id= :id", params, new OfferRowMapper());
		// return null;
	}

	public int deleteOffer(int id) {
		MapSqlParameterSource params = new MapSqlParameterSource("id", id);

		return jdbc.update("delete from offer where id=:id", params);
	}

	public boolean createObject(Offer offer) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(offer);

		return jdbc.update("insert into offers(name, text, email) values (:name, :text, :email)", params) == 1;
		
	}
	public void create(Offer offer) {
		// TODO Auto-generated method stub
		
	}
}
