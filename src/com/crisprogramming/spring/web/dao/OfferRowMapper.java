package com.crisprogramming.spring.web.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class OfferRowMapper implements RowMapper<Offer> {

	@Override
	public Offer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Offer offer = new Offer();

		offer.setId(rs.getInt("id"));
		offer.setName(rs.getString("name"));
		offer.setText(rs.getString("text"));
		offer.setEmail(rs.getString("email"));

		return offer;
	}


}
