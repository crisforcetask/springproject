/**
 * 
 */
package com.crisprogramming.spring.web.dao;

import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author cristina.smaranda
 *
 */
@Transactional
@Component("usersDao")
public class UsersDao {

	// private JdbcTemplate jdbc;
	private NamedParameterJdbcTemplate jdbc;
	// in order to encode the user's password during the user create method
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private SessionFactory sessionFactory;

	public UsersDao() {
		System.out.println("Successfully loaded usersrDAO");
	}
	//@Autowired
	public void setDataSource(DataSource jdbc) {
		// this.jdbc = new JdbcTemplate(jdbc);
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);

	}


	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Transactional
	public boolean create(User user) {
		//BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(user);
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("username", user.getUsername());
		params.addValue("password",passwordEncoder.encode(user.getUsername()));
		params.addValue("email", user.getEmail());
		params.addValue("enabled", user.isEnabled());
		params.addValue("authoity", user.getAuthority());
		
		jdbc.update("insert into users(username, password, email,enabled) values (:username, :password, :email, :enabled)", params);
		return jdbc.update("insert into authorities(username, authority) values (:username, :authoity)", params) == 1;
		
	}
	public boolean exists(String username) {	
		return jdbc.queryForObject("select count(*) from users where username=:username", new MapSqlParameterSource("username", username) , Integer.class) > 0;
	}
	
	public List<User> getAllUsers() {
		return getSession().createQuery("from Users ").list();
		//return jdbc.query("select * from users, authorities where users.username = authorities.username", BeanPropertyRowMapper.newInstance(User.class));
	}
}
