package com.crisprogramming.spring.web.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.crisprogramming.spring.web.dao.User;
import com.crisprogramming.spring.web.service.UserService;

@Controller
public class LoginController {

	private UserService userService;

	@Autowired
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping("/login")
	public String showLogin() {
		return "login";
	}
	
	@RequestMapping("/denied")
	public String showDenied() {
		return "denied";
	}
	
//
//	@RequestMapping("/admin")
//	public String showAdmin(Model model) {
//
////		try {
////			List<User> users = userService.getAllUsers();
////			model.addAttribute("users", users);
////		} catch (AccessDeniedException e) {
////			throw new AccessDeniedException("AccessDeniedException");
////		}
//		return "admin";
//	}

	@RequestMapping("/newaccount")
	public String showNewAccount(Model model) {		
		model.addAttribute("user", new User());
		return "newaccount";
	}

	@RequestMapping(value = "/createaccount", method = RequestMethod.POST)
	public String createAccount(@Valid User user, BindingResult result) {

		if (result.hasErrors()) {		

			return "newaccount";
		}
		user.setAuthority("ROLE_USER");
		user.setEnabled(true);

		// Check if the username already exists in DB
		if (userService.exists(user.getUsername())) {
			result.rejectValue("username", "DuplicateKey.user.username");
			return "newaccount";
		}

		// Check if the username already exists in DB
		try {
			userService.create(user);
		} catch (DuplicateKeyException e) {
			// System.out.println(e.getClass());
			result.rejectValue("username", "DuplicateKey.user.username");
			return "newaccount";
		}

		System.out.println("----------salve userul in baza de date is dau mesaj de succes");

		return "accountcreated";
	}

	@RequestMapping("/loggedout")
	public String showLoggedOut() {
		return "loggedout";
	}
}
