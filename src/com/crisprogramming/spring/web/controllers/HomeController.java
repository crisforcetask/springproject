/**
 * 
 */
package com.crisprogramming.spring.web.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author cristina.smaranda
 *
 */
@Controller
public class HomeController {
	
	private static Logger logger = Logger.getLogger(HomeController.class);

	@RequestMapping("/")
	public String showHome() {
		logger.debug("Showing home message ....");
		return "home";
	}

	@RequestMapping("/admin")
	public String showAdmin() {
		return "admin";
	}

}
