package com.crisprogramming.spring.web.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.crisprogramming.spring.web.dao.Offer;
import com.crisprogramming.spring.web.service.OfferService;

@Controller
public class OffersController {

	/*
	 * @RequestMapping("/") public ModelAndView showHome(HttpSession session) {
	 * 
	 * ModelAndView modelAndView = new ModelAndView("home"); Map<String, Object>
	 * model = modelAndView.getModel(); model.put("name", "<b>Cris</b>");
	 * 
	 * return modelAndView; }
	 * 
	 */

	private OfferService offerService;

	@RequestMapping("/offers")
	public String showOffers(Model model) {

		offerService.throwTestException();

		List<Offer> offers = offerService.getCurrent();
		model.addAttribute("offers", offers);

		return "offers";
	}

	@RequestMapping("/createoffer")
	public String createOffers(Model model) {

		model.addAttribute("offer", new Offer());

		return "createoffer";
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String showTest(Model model, @RequestParam("id") String id) {
		System.out.println("received id =" + id);
		return "home";
	}

	@RequestMapping(value = "/doCreateOffer", method = RequestMethod.POST)
	public String doCreateOffer(Model model, @Valid Offer offer, BindingResult result) {

		if (result.hasErrors()) {
			// System.out.println(" Form does not validate out");
			// List<ObjectError> errors = result.getAllErrors();
			// for (ObjectError error : errors) {
			// System.out.println(error.getDefaultMessage());
			//
			// }
			return "createoffer";
		}

		return "createdOffer";
	}

	public OfferService getOffersService() {
		return offerService;
	}

	@Autowired
	public void setOffersService(OfferService offerService) {
		this.offerService = offerService;
	}

//	@ExceptionHandler(DataAccessException.class)
//	public String handleDataBAseException(DataAccessException ex) {
//		return "error";
//	}
}
