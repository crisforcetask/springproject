<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/main.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/script/jquery.js"></script>
<script type="text/javascript">
	function onLoad() {
		$("#idPassword").keyup(checkPasswordMarch);
		$("#idconfPassword").keyup(checkPasswordMarch);
		//$("#details").submit();
	}

	function canSubmit() {
		var password = $("#idPassword").val();
		var idconfPassword = $("#idconfPassword").val();
		if (password != idconfPassword) {
			return false;
		} else {
			return false;
		}
	}
	function checkPasswordMarch() {
		var password = $("#idPassword").val();
		var idconfPassword = $("#idconfPassword").val();
		if (password.length > 3 || idconfPassword.length > 3) {

			if (password == idconfPassword) {
				$("#passwordMatch").text("<fmt:message key='MatchedPasswords.user.passwor'></fmt:message>");
				$("#passwordMatch").addClass("valid");
				$("#passwordMatch").removeClass("error");
			} else {
				$("#passwordMatch").text("<fmt:message key='UnmatchedPasswords.user.password'></fmt:message>");
				$("#passwordMatch").addClass("error");
				$("#passwordMatch").removeClass("valid");
			}
		}

	}

	$(document).ready(onLoad);
</script>


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New account</title>
</head>
<body>


	<sf:form method="post" id="details"
		action="${pageContext.request.contextPath }/createaccount"
		commandName="user">
		<table class="formtable">
			<tr>
				<td class="label">Username:</td>
				<td><sf:input class="control" name="username" path="username"
						type="text" /><br />
					<div class="error">
						<sf:errors path="username"></sf:errors>
					</div></td>
			</tr>
			<tr>
				<td class="label">Email:</td>
				<td><sf:input class="control" name="email" path="email"
						type="text" /><br />
					<div class="error">
						<sf:errors path="email"></sf:errors>
					</div></td>
			</tr>
			<tr>
				<td class="label">Password:</td>
				<td><sf:input class="control" name="password" id="idPassword"
						path="password" type="text" /><br />
					<div class="error">
						<sf:errors path="password"></sf:errors>
					</div></td>
			</tr>
			<tr>
				<td class="label">Confirm password:</td>
				<td><input class="control" name="confirmpass"
					id="idconfPassword" type="text" />
					<div id="passwordMatch"></div>
					<div class="error">
						<sf:errors path="password" />
					</div></td>
			</tr>
			<tr>
				<td></td>
				<td><input class="control" value="Create advert" type="submit" /></td>
			</tr>
		</table>
	</sf:form>
</body>
</html>